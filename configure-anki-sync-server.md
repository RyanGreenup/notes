This article describes how to configure a sync server for anki.

I needed to install the [anki
server](https://docs.ankiweb.net/sync-server.html) but I wanted to
install it in a docker container.

First write a `Dockerfile`:

    FROM alpine as alpine

    # A command needs to be running or docker exits
    CMD tail -f /dev/null

Build that `Dockerfile`:

``` bash
docker build -t anki-sync-server .
```

Now write a `docker-compose.yml`:

``` yaml
services:
  anki:
    image: "anki-sync-server"
```

Make sure to

1.  Use the same image name as the `docker build` command in the `.yml`
2.  **disable** any VPNs, jumping in and out of docker containers with
    *Wiregurd* enabled, blocked docker from accessing the internet.

Spin up the container and jump inside:

``` bash
docker-compose -p anki up
docker container list | grep anki

# Get the name of the continer
doas docker container list | rev | awk '{print $1}' | rev

# Jump inside
docker container exec -it anki-anki-1 /bin/sh
```

Now run through the commands of setting up the service and write them
down in the `Dockerfile`, for me that was

``` bash
apk add git bash curl
# https://github.com/pyenv/pyenv#installation
curl https://pyenv.run | bash

# Run pyenv required commands
export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
```

At this stage I found alpine wasn't going to work for anki, so I elected
to use `void`:

- =docker-compose.yml-
      FROM voidlinux/voidlinux as void

      # A command needs to be running or docker exits
      CMD tail -f /dev/null
- `Dockerfile`
  ``` yaml
  services:
  anki:
      image: "anki-sync-server"
  ```

The following commands got the sync server running on that arch image:

``` bash
yes | pacman -Sy archlinux-keyring reflector glibc
reflector
yes | pacman -Syu
yes | pacman -S   base-devel python
python3 -m venv anki-sync-server
anki-sync-server/bin/pip install anki
SYNC_BASE=/anki-sync-data SYNC_HOST=0.0.0.0 SYNC_PORT=8746 SYNC_USER1=ryan:ryan123 anki-sync-server/bin/python -m anki.syncserver
```

Turning that into a Dockerfile:

``` docker
FROM archlinux
RUN yes | pacman -Sy archlinux-keyring reflector glibc
RUN reflector
RUN yes | pacman -Syu
RUN yes | pacman -S   base-devel python
RUN python3 -m venv anki-sync-server
RUN anki-sync-server/bin/pip install anki
# CMD is what the image runs when started
CMD SYNC_BASE=/anki-sync-data SYNC_HOST=0.0.0.0 SYNC_PORT=8746 SYNC_USER1=ryan:ryan123 anki-sync-server/bin/python -m anki.syncserver
```

Now build and deploy the dockerfile:

``` bash
docker build -t anki-sync-server .
doas docker-compose up -d
```

It's also possible to combine these two steps and ask the compose file
to build the container [^1]:

``` yaml
services:
  anki:
    build: .
    image: "anki-sync-server"
```

However, thiere is currently \[2023-09-04 Mon\] a bug with that so it
doesn't work at the moment.

<span id="use-an-appropriate-image"></span>

## Use an Appropriate Image

A better approach would be to use a docker container built specifically
for Python, like the official python docker container [^2], this would
be quicker to build and easier to maintain (also Alpine is lighter), in
that case the Dockerfile would simply be:

``` docker
FROM python
RUN python3 -m venv anki-sync-server
RUN anki-sync-server/bin/pip install anki[syncserver]
# CMD is what the image runs when started
CMD SYNC_BASE=/anki-sync-data SYNC_HOST=0.0.0.0 SYNC_PORT=8746 SYNC_USER1=ryan:ryan123 anki-sync-server/bin/python -m anki.syncserver
```

<span id="volumes-and-ports"></span>

# Volumes and Ports

To set the volume in the compose file, make sure to use an absolute path
for the docker directory in the `SYNC_BASE` variable, then modify the
`docker-compose.yml` to include the port and the data.

``` yaml
services:
  anki:
    # build: .
    image: "anki-sync-server"
    restart: always
    volumes:
      - ./data:/anki-sync-data
    ports:
      - 8746:8746
```

<span id="footnotes"></span>

<references />

[^1]: [Try Docker Compose \| Docker
    Docs](https://docs.docker.com/compose/gettingstarted/)

[^2]: [Docker](https://hub.docker.com/_/python)
