# Absolute and Squared Deviation

In statistics, there are several measures of dispersion which help describe the spread of a dataset, examples include IQR, MAD and variance.

## Summary
Fundamentally, the reason we take square differencess is because of the Gaussian Distribution.

  * Variance is a parameter that naturally falls out of the Gaussian Distribution
    * Where reasonable and simple assumptions are made that concern $\left| x_i - \mu \right|$ and mean values but not squared deviations
  * In Regression problems we aim to maximize the likelihood that the residuals come from a Gaussian Distribution, it just so happens this is equivalent to minimizing squared residuals (RSS).


## Definitions
MAD and variance are defined:

$$
\begin{aligned}
s & =\frac{1}{n-1}\sum_{i=1}^{n}\left[\left(x_{i}-\bar{x}\right)^{2}\right]\\
\sigma & =\frac{1}{n}\sum_{i=1}^{N}\left[\left(x_{i}-\mu\right)^{2}\right]\\
{\rm MAD} & =\frac{1}{n}\sum_{i=1}^{N}\left[\left|x_{i}-\mu\right|\right]
\end{aligned}
$$

```r
data <- rnorm(10^3, mean=7, sd=9)

## Variance calculation
variance <- var(data)

## Mean Absolute Deviation calculation
mad <- mean(abs(data - mean(data)))

print(paste("Variance: ", variance))
print(paste("Mean Absolute Deviation: ", mad))
```

    [1] "Variance:  79.487628132778"
    [1] "Mean Absolute Deviation:  7.09034007881871"

## Statistical Properties of Variance

The variance of the sum of two independent random variables is equal to sum of
their variances, this is not true for MAD (much in the same way that we cannot
calculate the distance between two points as the sum of their perpendicular distances, i.e. Pythagoras Theorem).

Variance and Mean are the only two parameters of the Gaussian Distribution, these two values characterize that shape entirely. To clarify this point, a rectangle is characterized by it's height and width, with those values it can be perfectly recreated. In the same way a Gaussian Distribution can be perfectly recreated from $\mu$ and $\sigma$.


## The Histor of the Normal Law of Errors (The Gaussian Distribution)
The Gaussian Distribution, also known as the Normal distribution (and colloquially known as a Bell Curve), has a rich history that shows its significance in statistical analysis. It is the only distribution that justifies the choice of arithmetic mean as the best estimate of the population mean via Maximum Likelihood Estimation (MLE), Gauss described this as the *Normal Law of Errors* in 1809 [^3]. Another way to phrase this is that the Gaussian Distribution is the only probability distribution that has a higher probability of seeing values closer to the mean such that the sample mean is the best estimate of the population mean.

Laplace was an early contributor to the development of this distribution, in 1782, he showed that  $\int e^{-t^2} \mathrm{d}t = \sqrt{\pi}$ in 1782 [^4] and later developed the Central Limit Theorem, emphasizing the importance of the Gaussian Distribution in Statistics [^5]. Maxwell also demonstrated the occurence of this distribution in nature, showing that it described the number of particles with a given velocity in his experiments [^6].

Interestingly, Laplace attempted to show that the arithmetic mean was the best estimate of the error function but failed, mainly because he mistakenly assumed that the Mean Absolute Deviation was the appropriate measure to quantify the spread of errors [^10].

In 1918 (according to the Wikipedia page on the Gaussian Distribution), Ronald Fisher played a pivotal role in popularizing the use of variance ($\sigma^2 = \frac{1}{N} \sum^n_{i=1}\left[ \left( x - \mu\right)^2 \right]$) (see [^2] [^3]). He justified the use of variance as a measure of spread, referring to the *Normal Law of Errors* [^2]:

> The great body of available statistics show us that the deviations of a human measurement from its mean follow very closely the Normal Law of Errors, and, therefore, that the variability may be uniformly measured by the standard deviation corresponding to the square root of the mean square error.

In the next paragraph, Fisher further argued in favour of variance over standard deviation or MAD because the variance of random distributions sums, as discussed earlier.

### Deriving the Normal Distribution


The Gaussian Distribution is underpinned by three assumptions [^post] concerned with sampling from population where the observations can take any real value


[^note]:  A uniform distribution can only return values within a given domain, so even though it nearly matches these definitions, the fact that it's defined for all values excludes it, more over condition 3 could be adjusted to exclude it, i.e. values further from the mean should be less likely

[^post]: https://alanhdu.github.io/posts/2019-10-21-normal-distribution-derivation/

1. Each observation is i.i.d. (independentally and identically distributed).
    * i.e. Each value sampled comes from the same population
    * That population has one distribution
    * So '''not''' a mixure of two different distributions, e.g. travel time of men and women
2. The sample mean, $\bar{x} = \sum^n_{i=1}\left[x_i\right]$, is the estimator of the population mean, $\mu$, which maximizes the likelihood of seeing the sample, i.e.:

      $\bar{x} = \underset{\hat{\mu}}{\mathrm{arg}\mathrm{max}} \left( \prod^n_{i=1}\left[p\left(x_i; \hat{\mu} \right) \right] \right)$

3. The probability of each measurement is a continuous function of its distance from the mean.
    * i.e. $p\left(x; \mu \right) = f\left( \left\lvert x- \mu \right\rvert \right)$ where $f$ is continuous.

The result of this, which is derived below is:

$$
\frac{\mathrm{d}}{\mathrm{d}x}\left(\log\left(f\left(\left|x-\bar{x}\right|\right)\right)\right)=\sigma\left(x_{i}-\bar{x}\right)
$$

which can be solved using techniques of Ordinary Differential Equations and Advanced Calculus to give:

$$
y =  k \cdot e^{\frac{z^2}{2}}
$$

where:
  * $k=\frac{1}{2\pi}$
  * $z=\frac{x-\mu}{\sigma}$
  * $\sigma = \mathrm{mean}\left( \left( x_i - \mu \right)^2 \right)$
    * $\mathrm{mean}\left(x\right)=\frac{1}{N}\sum^N_{i=1} \left[x_i\right]$

The derivation of this is somewhat outside the scope of this article, if you're curious see [^post] and/or message me. The techniques to do this will be developed in the subject *Advanced Statistical Methods*.


### Geometric Assumptions
These assumptions can be related by analogy to a geometric interpretation which ultimately provides something to the effect of $x \propto \left| x - \mu \right| \times x \propto \left( y \right) \implies y = \frac{1}{\sigma \sqrt{2 \pi}} e^{-\frac{1}{2}z^2}$ the resulting formula is $y \propto e^{z^2/2}$ where $z=\frac{x-\mu}{\sigma}$ where $\sigma = \rm{mean}\left(\left(x_i-\mu\right)^2\right)$, this is a result of pythagoras theorem.
#### Relationship to Pythagoras
One way to interpret the emergence of $\sigma$ is by relating it to Pythagoras Theorem, which provides that the square root of the squared distance (the $\mathcal{L}^2$ norm) is the best measure of distance in two dimensions, it just so happens to be true in higher dimensions as well.

## RSS as a Maximum Likelihood Estimator

When modeling data, we often assume that errors follow a normal distribution. In this context, the parameter values that maximize the likelihood of the errors adhering to a Gaussian distribtuion will be the same parameter values that minimize the squared errors. This assumption aligns with Gauss's suggestion [^3] that the sample mean should be the best estimate of the population mean (in which case the mean value of error would be zero).

However, if the residual values were not expected to follow a Gaussian distribution (e.g. fitting a linear regression to autocorrelated time-series data), the advantages of using squared deviations over absolute deviations become more practical than theoretical. MAD offers greater robustness to outliers, alghough it is less sensitive to small shifts in data, this is one practical consideration when deciding upon a loss function in the absence of theoretical grounding.


### Other Misc Properties of Variance
Another nice property is $\sigma^2 = \mathtt{mean}\left(X^2\right) - \mathtt{mean}\left(X\right)^2$.

## Footnotes

[^3]: Gauss, Carolo Friderico (1809). Theoria motvs corporvm coelestivm in sectionibvs conicis Solem ambientivm [Theory of the Motion of the Heavenly Bodies Moving about the Sun in Conic Sections] (in Latin). English translation.

[^2]: Fisher, R. A. “XV.—The Correlation between Relatives on the Supposition of Mendelian Inheritance.” Earth and Environmental Science Transactions of The Royal Society of Edinburgh 52, no. 2 (ed 1919): 399–433. https://doi.org/10.1017/S0080456800012163.


[^6]: Maxwell, J. C. “Illustrations of the Dynamical Theory of Gases.—Part I. On the Motions and Collisions of Perfectly Elastic Spheres.” The London, Edinburgh, and Dublin Philosophical Magazine and Journal of Science 19, no. 124 (January 1860): 19–32. https://doi.org/10.1080/14786446008642818.

[^5]: Stigler, Stephen M. The History of Statistics: The Measurement of Uncertainty before 1900. Cambridge, Mass: Belknap Press of Harvard University Press, 1986. M. Stigler - The History of Statistics_ The Measurement of Uncertainty Before 1900,-Belknap Press (1986).pdf The History of Statistics

[^4]: Pearson, Karl. “‘Das Fehlergesetz Und Seine Verallgemeinerungen Durch Fechner Und Pearson.’ A Rejoinder.” Biometrika 4, no. 1/2 (June 1905): 169. https://doi.org/10.2307/2331536. Archive:pearsonFehlergesetzUndSeine1905

[^3]:  Gauss, Carolo Friderico (1809). Theoria motvs corporvm coelestivm in sectionibvs conicis Solem ambientivm [Theory of the Motion of the Heavenly Bodies Moving about the Sun in Conic Sections] (in Latin). English translation.

[^10]: https://link.springer.com/content/pdf/10.1007/978-0-387-46409-1_7.pdf
