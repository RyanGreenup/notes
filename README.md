# Notes

* Statistics
  * [Absolute and Squared Deviation](./absolute-and-squared-distance.md)
    * [Working Example](./Biased Variance.ipynb)
  * [Bessel's Correction](./bessels_correction.md)

